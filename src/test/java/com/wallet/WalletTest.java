package com.wallet;

import com.wallet.currencies.*;
import com.wallet.exceptions.InvalidCurrencyException;
import com.wallet.exceptions.MoneyLimitExceededException;
import com.wallet.exceptions.MoneyShouldBePositiveException;
import org.apache.commons.math3.util.Precision;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class WalletTest {

    @Test
    void shouldReturnTrueWhen74point85RupeesEqualToOneDollar() throws MoneyShouldBePositiveException {
        double RupeeValue = 74.54;
        double dollarValue = 1.0;
        Currency rupees = new Rupee(RupeeValue);
        Currency dollars = new Dollar(dollarValue);
        Wallet wallet = new Wallet();

        wallet.creditCurrency(rupees);
        wallet.creditCurrency(dollars);
        double actualValue1 = rupees.convert(dollars);
        double actualValue2 = dollars.convert(rupees);

        assertAll(() -> assertThat(dollars.getValue(), is(equalTo(actualValue1))),
                () -> assertThat(rupees.getValue(), is(equalTo(actualValue2))));
    }

    @Test
    void shouldThrowMoneyShouldBePositiveExceptionWhenEitherRupeeOrDollarIsNegative() {
        double RupeeValue = -74.54;
        double dollarValue = -1.0;

        Executable rupees = () -> new Rupee(RupeeValue);
        Executable dollars = () -> new Dollar(dollarValue);

        assertAll(() -> assertThrows(MoneyShouldBePositiveException.class, dollars),
                () -> assertThrows(MoneyShouldBePositiveException.class, rupees));
    }

    @Test
    void shouldReturnBalanceInPreferredCurrencyAsRupees() throws MoneyShouldBePositiveException, InvalidCurrencyException {
        double expectedTotal = 223.62;
        double dollarValue = 1.0;
        double euroValue = 0.83;
        double rupeeValue = 74.54;
        Wallet wallet = new Wallet();
        Currency dollars = new Dollar(dollarValue);
        Currency euros = new Euro(euroValue);
        Currency rupees = new Rupee(rupeeValue);

        wallet.creditCurrency(dollars);
        wallet.creditCurrency(euros);
        wallet.creditCurrency(rupees);
        double actualTotal = Precision.round(wallet.totalInPreferredCurrency(rupees), 2);

        assertAll(() -> assertThat(expectedTotal, is(equalTo(actualTotal))));
    }

    @Test
    void shouldReturnBalanceInPreferredCurrencyInDollar() throws MoneyShouldBePositiveException, InvalidCurrencyException {
        double expectedTotal = 3.0;
        double rupeeValue = 74.54;
        double euroValue = 0.83;
        double dollarValue = 1.0;

        Wallet wallet = new Wallet();
        Currency rupees = new Rupee(rupeeValue);
        Currency euros = new Euro(euroValue);
        Currency dollars = new Dollar(dollarValue);

        wallet.creditCurrency(rupees);
        wallet.creditCurrency(euros);
        wallet.creditCurrency(dollars);
        double actualTotal = Precision.round(wallet.totalInPreferredCurrency(dollars), 2);

        assertThat(expectedTotal, is(equalTo(actualTotal)));
    }

    @Test
    void shouldThrowInvalidCurrencyExceptionWhenPreferredCurrencyIsNotPresentInWallet() throws MoneyShouldBePositiveException {
        double rupeeValue = 74.54;
        double euroValue = 0.83;
        double dollarValue = 10;
        Wallet wallet = new Wallet();
        Currency rupees = new Rupee(rupeeValue);
        Currency euros = new Euro(euroValue);
        Currency dollars = new Dollar(dollarValue);

        wallet.creditCurrency(rupees);
        wallet.creditCurrency(euros);
        Executable executable = () -> wallet.totalInPreferredCurrency(dollars);

        assertThrows(InvalidCurrencyException.class, executable);
    }

    @Test
    void shouldPutMoneyIntoTheWallet() throws MoneyShouldBePositiveException {
        double rupeeValue = 74.54;
        double euroValue = 0.83;
        double expected1 = 149.08;
        double expected2 = 0.83;
        Wallet wallet = new Wallet();
        Currency rupees = new Rupee(rupeeValue);
        Currency euros = new Euro(euroValue);

        wallet.creditCurrency(rupees);
        wallet.creditCurrency(rupees);
        wallet.creditCurrency(euros);

        assertAll(() -> assertThat(expected1, is(equalTo(wallet.getIndividualTotal(rupees)))),
                () -> assertThat(expected2, is(equalTo(wallet.getIndividualTotal(euros)))));
    }

    @Test
    void shouldTakeMoneyFromTheWalletWhenWalletHasEnoughMoney() throws MoneyShouldBePositiveException, MoneyLimitExceededException, InvalidCurrencyException {
        double expected1 = 10;
        double rupeeValue1 = 20;
        double rupeeValue2 = 10;
        Wallet wallet = new Wallet();
        Currency rupees1 = new Rupee(rupeeValue1);
        Currency rupees2 = new Rupee(rupeeValue2);

        wallet.creditCurrency(rupees1);
        wallet.creditCurrency(rupees2);
        wallet.debitCurrency(rupees1);

        assertThat(expected1, is(equalTo(wallet.getIndividualTotal(rupees1))));
    }

    @Test
    void shouldThrowMoneyLimitExceededExceptionWhenTheWalletDoesNotHaveEnoughMoney() throws MoneyShouldBePositiveException {
        double rupeeValue1 = 10;
        double rupeeValue2 = 20;
        Wallet wallet = new Wallet();
        Currency rupees1 = new Rupee(rupeeValue1);
        Currency rupees2 = new Rupee(rupeeValue2);

        wallet.creditCurrency(rupees1);
        Executable executable = () -> wallet.debitCurrency(rupees2);

        assertThrows(MoneyLimitExceededException.class, executable);
    }

    @Test
    void shouldReturnBalanceInPreferredCurrencyInPounds() throws MoneyShouldBePositiveException, InvalidCurrencyException {
        double expectedTotal = 3.0;
        double rupeeValue = 103.52;
        double dollarValue = 1.38;
        double poundValue = 1.0;
        Wallet wallet = new Wallet();
        Currency rupees = new Rupee(rupeeValue);
        Dollar dollars = new Dollar(dollarValue);
        Currency pounds = new Pound(poundValue);

        wallet.creditCurrency(rupees);
        wallet.creditCurrency(dollars);
        wallet.creditCurrency(pounds);
        double actualTotal = Precision.round(wallet.totalInPreferredCurrency(pounds), 2, 2);

        assertThat(expectedTotal, is(equalTo(actualTotal)));
    }

    @Test
    void shouldReturnBalanceInPreferredCurrencyInSingaporeDollar() throws MoneyShouldBePositiveException, InvalidCurrencyException {
        double expectedTotal = 4.0;
        double rupeeValue = 56.04;
        double dollarValue = 0.75;
        double poundValue = 0.54;
        double singaporeDollarValue = 1.0;
        Wallet wallet = new Wallet();
        Currency rupees = new Rupee(rupeeValue);
        Currency dollars = new Dollar(dollarValue);
        Currency pounds = new Pound(poundValue);
        Currency singaporeDollar = new SingaporeDollar(singaporeDollarValue);

        wallet.creditCurrency(rupees);
        wallet.creditCurrency(dollars);
        wallet.creditCurrency(pounds);
        wallet.creditCurrency(singaporeDollar);
        double actualTotal = Precision.round(wallet.totalInPreferredCurrency(singaporeDollar), 2, 2);

        assertThat(expectedTotal, is(equalTo(actualTotal)));
    }

    @Test
    void shouldReturnBalanceInPreferredCurrencyInEuro() throws MoneyShouldBePositiveException, InvalidCurrencyException {
        double expectedTotal = 5.0;
        double rupeeValue = 89.97;
        double dollarValue = 1.20;
        double poundValue = 0.87;
        double singaporeValue = 1.60;
        double euroValue = 1.0;
        Wallet wallet = new Wallet();
        Currency rupees = new Rupee(rupeeValue);
        Currency dollars = new Dollar(dollarValue);
        Currency pounds = new Pound(poundValue);
        Currency singaporeDollars = new SingaporeDollar(singaporeValue);
        Currency euros = new Euro(euroValue);

        wallet.creditCurrency(rupees);
        wallet.creditCurrency(dollars);
        wallet.creditCurrency(pounds);
        wallet.creditCurrency(singaporeDollars);
        wallet.creditCurrency(euros);

        double actualTotal = Precision.round(wallet.totalInPreferredCurrency(euros), 2, 2);

        assertThat(expectedTotal, is(equalTo(actualTotal)));
    }

    @Test
    void shouldPutMoneyIntoTheWalletWhenSingaporeDollarAndEuroIsAdded() throws MoneyShouldBePositiveException {
        double expectedTotal1 = 3.0;
        double expectedTotal2 = 2.0;
        double euroValue = 2.0;
        double singaporeValue = 1.5;
        Wallet wallet = new Wallet();
        Currency singaporeDollars = new SingaporeDollar(singaporeValue);
        Currency euros = new Euro(euroValue);

        wallet.creditCurrency(singaporeDollars);
        wallet.creditCurrency(singaporeDollars);
        wallet.creditCurrency(euros);

        assertAll(() -> assertThat(expectedTotal1, is(equalTo(wallet.getIndividualTotal(singaporeDollars)))),
                () -> assertThat(expectedTotal2, is(equalTo(wallet.getIndividualTotal(euros)))));
    }
}
