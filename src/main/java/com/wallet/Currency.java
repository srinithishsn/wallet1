package com.wallet;

import com.wallet.exceptions.MoneyShouldBePositiveException;

public class Currency {
    protected double value;
    protected final String symbol;
    protected final double valueInOneDollar;

    public Currency(double value, String symbol, double valueInOneDollar) throws MoneyShouldBePositiveException {
        if (value < 0)
            throw new MoneyShouldBePositiveException();
        this.value = value;
        this.symbol = symbol;
        this.valueInOneDollar = valueInOneDollar;
    }

    public String getSymbol() {
        return symbol;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public Double convert(Currency currency) {
        return value * (currency.valueInOneDollar / this.valueInOneDollar);
    }
}
