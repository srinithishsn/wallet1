package com.wallet.currencies;

import com.wallet.Currency;
import com.wallet.exceptions.MoneyShouldBePositiveException;

public class Pound extends Currency {
    public Pound(double value) throws MoneyShouldBePositiveException {
        super(value, "£", 0.72);
    }
}
