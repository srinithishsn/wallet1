package com.wallet.currencies;

import com.wallet.Currency;
import com.wallet.exceptions.MoneyShouldBePositiveException;

public class Dollar extends Currency {
    public Dollar(double value) throws MoneyShouldBePositiveException {
        super(value, "$", 1.0);
    }
}
