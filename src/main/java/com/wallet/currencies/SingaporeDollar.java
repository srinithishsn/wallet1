package com.wallet.currencies;

import com.wallet.Currency;
import com.wallet.exceptions.MoneyShouldBePositiveException;

public class SingaporeDollar extends Currency {
    public SingaporeDollar(double value) throws MoneyShouldBePositiveException {
        super(value, "S$", 1.33);
    }
}
