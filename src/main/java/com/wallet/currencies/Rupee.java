package com.wallet.currencies;

import com.wallet.Currency;
import com.wallet.exceptions.MoneyShouldBePositiveException;

public class Rupee extends Currency {
    public Rupee(double value) throws MoneyShouldBePositiveException {
        super(value, "₹", 74.54);
    }
}
