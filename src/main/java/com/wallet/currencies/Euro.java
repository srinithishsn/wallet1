package com.wallet.currencies;

import com.wallet.Currency;
import com.wallet.exceptions.MoneyShouldBePositiveException;

public class Euro extends Currency {
    public Euro(double value) throws MoneyShouldBePositiveException {
        super(value, "€", 0.83);
    }
}
