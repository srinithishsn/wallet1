package com.wallet;

import com.wallet.exceptions.InvalidCurrencyException;
import com.wallet.exceptions.MoneyLimitExceededException;

import java.util.ArrayList;
import java.util.List;

public class Wallet {

    private final List<Currency> currencies = new ArrayList<>();

    public Wallet() {
    }

    public void creditCurrency(Currency currency) {
        currencies.add(currency);
    }

    public void debitCurrency(Currency currency) throws MoneyLimitExceededException, InvalidCurrencyException {
        if (getIndividualTotal(currency) < currency.getValue())
            throw new MoneyLimitExceededException();
        for (Currency entry : currencies) {
            if (entry.equals(currency)) {
                entry.setValue(entry.getValue() - currency.getValue());
                return;
            }
        }
        currencies.add(currency);
    }

    public Double totalInPreferredCurrency(Currency preferredCurrency) throws InvalidCurrencyException {
        double total = 0;
        if (!currencies.contains(preferredCurrency)) {
            throw new InvalidCurrencyException();
        }
        for (Currency entry : currencies) {
            if (entry.getSymbol().equals(preferredCurrency.getSymbol()))
                total += entry.getValue();
            else
                total += entry.convert(preferredCurrency);
        }
        return total;
    }

    public Double getIndividualTotal(Currency currency) throws InvalidCurrencyException {
        double total = 0;
        boolean currencyPresent = false;
        for (Currency entry : currencies) {
            if (entry.getSymbol().equals(currency.getSymbol())) {
                total += entry.getValue();
                currencyPresent = true;
            }
        }
        if (!currencyPresent)
            throw new InvalidCurrencyException();
        return total;
    }

}
